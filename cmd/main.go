package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"open_library/internal/delivery/rest_api"
	"open_library/internal/delivery/rest_api/book_http"
	"open_library/internal/delivery/rest_api/operation_http"
	"open_library/internal/repository/book_repo"
	"open_library/internal/repository/operation_repo"
	"open_library/internal/seeder"
	"open_library/internal/usecase/book_uc"
	"open_library/internal/usecase/operation_uc"
	"os"
	"time"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = ":8080"
	}

	r := mux.NewRouter()
	r.HandleFunc("/", rest_api.HomeHandler).Methods("GET")

	bookRepo := book_repo.NewBookRepository(seeder.GenerateBooks())
	bookUC := book_uc.NewBook(bookRepo)
	operationRepo := operation_repo.NewBookRepository()
	operationUC := operation_uc.NewOperation(operationRepo, bookRepo)

	book_http.BookHandler(r, bookUC)
	operation_http.OperationHandler(r, operationUC)

	http.Handle("/", r)

	srv := &http.Server{
		Handler:      r,
		Addr:         port,
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
