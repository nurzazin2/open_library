package repository

import "open_library/domain/entity"

type BookRepo interface {
	FindListBook(subject string) ([]*entity.Book, error)
	FindBook(slug string) (*entity.Book, error)
}
