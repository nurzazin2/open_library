package repository

import "open_library/domain/entity"

type OperationRepo interface {
	CreateOperationBorrow(b *entity.OperationBorrow) error
	FindList() ([]*entity.OperationBorrow, error)
}
