package entity

import "open_library/pkg/common"

type Subject struct {
	Id    common.ID
	Slug  string
	Title string
}
