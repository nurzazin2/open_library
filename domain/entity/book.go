package entity

import "open_library/pkg/common"

type Book struct {
	Id            common.ID
	Slug          string
	Title         string
	Subject       *Subject
	Author        string
	EditionNumber string
}
