package entity_test

import (
	"github.com/bxcodec/faker/v3"
	"open_library/domain/entity"
	"open_library/pkg/common"
	"open_library/pkg/utils"
	"open_library/test/testdata"
	"reflect"
	"testing"
	"time"
)

func TestNewOperationBorrow(t *testing.T) {
	type args struct {
		dto entity.OperationBorrowDTO
	}

	fakeDataOperationBorrow := testdata.NewOperationBorrow(faker.Word())
	borrowDateYesterday := time.Now().Add(-24 * time.Hour)

	tests := []struct {
		name    string
		args    args
		want    *entity.OperationBorrow
		wantErr bool
	}{
		{
			name: "OperationBorrowOK",
			args: args{
				dto: entity.OperationBorrowDTO{
					Id:          fakeDataOperationBorrow.Id,
					Borrower:    fakeDataOperationBorrow.Borrower.Name,
					Book:        fakeDataOperationBorrow.Book.Slug,
					BorrowDate:  utils.TimeToDate(fakeDataOperationBorrow.BorrowDate),
					SubmittedAt: fakeDataOperationBorrow.SubmittedAt,
				},
			},
			want:    fakeDataOperationBorrow,
			wantErr: false,
		},
		{
			name: "OperationBorrowErrorInvalidBorrowDate",
			args: args{
				dto: entity.OperationBorrowDTO{
					Id:          fakeDataOperationBorrow.Id,
					Borrower:    fakeDataOperationBorrow.Borrower.Name,
					Book:        fakeDataOperationBorrow.Book.Slug,
					BorrowDate:  utils.TimeToDate(borrowDateYesterday),
					SubmittedAt: fakeDataOperationBorrow.SubmittedAt,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "OperationBorrowErrorRequiredBooks",
			args: args{
				dto: entity.OperationBorrowDTO{
					Id:          fakeDataOperationBorrow.Id,
					Borrower:    fakeDataOperationBorrow.Borrower.Name,
					Book:        "",
					BorrowDate:  utils.TimeToDate(fakeDataOperationBorrow.BorrowDate),
					SubmittedAt: fakeDataOperationBorrow.SubmittedAt,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "OperationBorrowErrorRequiredName",
			args: args{
				dto: entity.OperationBorrowDTO{
					Id:          fakeDataOperationBorrow.Id,
					Borrower:    "",
					Book:        fakeDataOperationBorrow.Book.Slug,
					BorrowDate:  utils.TimeToDate(fakeDataOperationBorrow.BorrowDate),
					SubmittedAt: fakeDataOperationBorrow.SubmittedAt,
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := entity.NewOperationBorrow(tt.args.dto)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewOperationBorrow() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOperationBorrow() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOperationBorrow_AddBook(t *testing.T) {
	type fields struct {
		Id          common.ID
		Borrower    entity.User
		Book        *entity.Book
		BorrowDate  time.Time
		SubmittedAt time.Time
	}
	type args struct {
		book *entity.Book
	}

	fakeBook := testdata.NewBook()
	fakeDataOperationBorrow := testdata.NewOperationBorrow(fakeBook.Slug)

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "AddBookOK",
			fields: fields{
				Id:          fakeDataOperationBorrow.Id,
				Borrower:    fakeDataOperationBorrow.Borrower,
				Book:        fakeDataOperationBorrow.Book,
				BorrowDate:  fakeDataOperationBorrow.BorrowDate,
				SubmittedAt: fakeDataOperationBorrow.SubmittedAt,
			},
			args: args{
				book: fakeBook,
			},
			wantErr: false,
		},
		{
			name: "AddBookErrorNil",
			fields: fields{
				Id:          fakeDataOperationBorrow.Id,
				Borrower:    fakeDataOperationBorrow.Borrower,
				Book:        fakeDataOperationBorrow.Book,
				BorrowDate:  fakeDataOperationBorrow.BorrowDate,
				SubmittedAt: fakeDataOperationBorrow.SubmittedAt,
			},
			args: args{
				book: nil,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &entity.OperationBorrow{
				Id:          tt.fields.Id,
				Borrower:    tt.fields.Borrower,
				Book:        tt.fields.Book,
				BorrowDate:  tt.fields.BorrowDate,
				SubmittedAt: tt.fields.SubmittedAt,
			}
			if err := b.AddBook(tt.args.book); (err != nil) != tt.wantErr {
				t.Errorf("AddBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
