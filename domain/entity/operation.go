package entity

import (
	"errors"
	"open_library/pkg/common"
	"open_library/pkg/exceptions"
	"open_library/pkg/utils"
	"time"
)

type OperationBorrowDTO struct {
	Id          common.ID
	Borrower    string
	Book        string
	BorrowDate  string
	SubmittedAt time.Time
}

type OperationBorrow struct {
	Id          common.ID
	Borrower    User
	Book        *Book
	BorrowDate  time.Time
	SubmittedAt time.Time
}

func NewOperationBorrow(dto OperationBorrowDTO) (*OperationBorrow, error) {
	ob := &OperationBorrow{
		Id: dto.Id,
		Borrower: User{
			Name: dto.Borrower,
		},
		BorrowDate:  utils.DateToTime(dto.BorrowDate),
		Book:        &Book{Slug: dto.Book},
		SubmittedAt: utils.DateDefault(dto.SubmittedAt),
	}

	if ob.Book.Slug == "" {
		return nil, exceptions.ErrorRequired("Book")
	}
	if ob.Borrower.Name == "" {
		return nil, exceptions.ErrorRequired("Name")
	}
	if dto.BorrowDate == "" {
		return nil, exceptions.ErrorRequired("BorrowDate")
	}
	if ob.BorrowDate.IsZero() {
		return nil, exceptions.ErrorInvalid("BorrowDate")
	}

	now := time.Now().Local()
	todayInFirstHour := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	if ob.BorrowDate.Before(todayInFirstHour) {
		return nil, errors.New("BorrowDate must be >= today")
	}

	return ob, nil
}

func (b *OperationBorrow) AddBook(book *Book) error {
	if book == nil {
		return exceptions.ErrorInvalid("Book")
	}
	b.Book = book
	return nil
}
