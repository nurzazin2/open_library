package usecase

import (
	"open_library/domain/entity"
)

type OperationUseCase interface {
	BorrowBook(dto entity.OperationBorrowDTO) (*entity.OperationBorrow, error)
	BorrowList() ([]*entity.OperationBorrow, error)
}
