package usecase

import (
	"open_library/domain/entity"
)

type BookUseCase interface {
	FindBookList(subject string) ([]*entity.Book, error)
}
