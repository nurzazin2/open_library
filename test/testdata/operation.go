package testdata

import (
	"github.com/bxcodec/faker/v3"
	"open_library/domain/entity"
	"open_library/pkg/common"
	"open_library/pkg/utils"
	"time"
)

func NewOperationBorrow(book string) *entity.OperationBorrow {
	nowString := utils.TimeToDate(time.Now())
	return &entity.OperationBorrow{
		Id: common.NewID(),
		Borrower: entity.User{
			Name: faker.Name(),
		},
		Book:        &entity.Book{Slug: book},
		BorrowDate:  utils.DateToTime(nowString),
		SubmittedAt: time.Now(),
	}
}
