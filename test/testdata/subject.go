package testdata

import (
	"github.com/bxcodec/faker/v3"
	"github.com/gosimple/slug"
	"open_library/domain/entity"
	"open_library/pkg/common"
)

func NewSubject() *entity.Subject {
	title := faker.Word()
	return &entity.Subject{
		Id:    common.NewID(),
		Slug:  slug.Make(title),
		Title: title,
	}
}
