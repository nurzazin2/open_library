package testdata

import (
	"github.com/bxcodec/faker/v3"
	"github.com/gosimple/slug"
	"open_library/domain/entity"
	"open_library/pkg/common"
)

func NewBook() *entity.Book {
	title := faker.Word()
	return &entity.Book{
		Id:            common.NewID(),
		Slug:          slug.Make(title),
		Title:         title,
		Subject:       NewSubject(),
		Author:        faker.Name(),
		EditionNumber: "1",
	}
}
