# Open Library

***

## Description
This is only simple app for build open library

### How to build and run?
- open your terminal and run command below
- `go mod download`
- `go build -o ./bin ./cmd/main.go`
- then you need to run the binary file inside `bin` directory
- backend apis can be access via URL [http://localhost:8080](http://localhost:8080) on your favourite browser

### Endpoints
##### HomePage -> `GET http://localhost:8080`
##### GetListBooks -> `GET http://localhost:8080/books/horror`
- parameter -> example subject is `horror`
##### BorrowBook -> `POST http://localhost:8080/borrow`
- example body 
```json
{
  "borrower": "nur zazin",
  "book": "dracula",
  "borrow_date": "2022-09-18"
}
```
##### GetListBorrowBook -> `GET http://localhost:8080/borrow`

## Authors
- [nurza.cool@gmail.com](mailto:nurza.cool@gmail.com)

## External Modules
- [Faker](https://github.com/go-faker/faker)
- [UUID](https://github.com/google/uui)
- [Gorilla Mux](https://github.com/gorilla/mux)
- [Slug Generator](https://github.com/gosimple/slug)

### Project Structure

```
<root>
  ├── bin
  │   ├── <service_name_executable>
  │   └── ...
  │
  ├── cmd
  │   └── main.go
  │
  ├── domain
  │   ├── entity
  │   │   └── <domain_name>.go
  │   ├── repository
  │   │   └── <interface_repository_name>.go
  │   └── usecase
  │       └── <interface_usecase_name>.go
  │
  ├── internal
  │   ├── delivery
  │   │   └── rest_api
  │   │       └── <controller>.go
  │   ├── repository
  │   │   ├── <repo_implementation>.go
  │   │   └── <repo_implementation>_test.go
  │   │
  │   └── usecase
  │       ├── <usecase_implementation>.go
  │       └── <usecase_implementation>_test.go
  ├── pkg
  └── README.md
```

| No  | Folder Name                         | Purpose                                                                                                       |
|-----|-------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1.  | `bin`                               | Golang executable file                                                                                        |
| 2.  | `cmd`                               | Golang main program per service name                                                                          |
| 3.  | `domain/*/<*>.go`                   | Golang file that holds definition for data model, usecase interface, and repository interface for each domain |
| 4a. | `internal/delivery/rest_api/<*>.go` | Controller for mapping apis                                                                                   |
| 4b. | `internal/repository`               | Encapsulated Implementation of Repository Interface                                                           |
| 4c. | `internal/usecase`                  | Golang file that holds business logic implementation                                                          |
| 5.  | `pkg`                               | helper / common function                                                                                      |
