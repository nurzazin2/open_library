package request

type OperationBorrow struct {
	Borrower   string `json:"borrower"`
	Book       string `json:"book"`
	BorrowDate string `json:"borrow_date"`
}
