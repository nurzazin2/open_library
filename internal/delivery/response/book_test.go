package response_test

import (
	"open_library/domain/entity"
	"open_library/internal/delivery/response"
	"open_library/test/testdata"
	"reflect"
	"testing"
)

func TestDomainToRestBook(t *testing.T) {
	type args struct {
		book *entity.Book
	}

	fakeBook := testdata.NewBook()
	fakeResponse := &response.Book{
		Id:    fakeBook.Id.String(),
		Slug:  fakeBook.Slug,
		Title: fakeBook.Title,
		Subject: response.BookSubject{
			Slug:  fakeBook.Subject.Slug,
			Title: fakeBook.Subject.Title,
		},
		Author:        fakeBook.Author,
		EditionNumber: fakeBook.EditionNumber,
	}

	tests := []struct {
		name string
		args args
		want *response.Book
	}{
		{
			name: "DomainToRestBookOK",
			args: args{
				book: fakeBook,
			},
			want: fakeResponse,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := response.DomainToRestBook(tt.args.book); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DomainToRestBook() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDomainToRestBookList(t *testing.T) {
	type args struct {
		books []*entity.Book
	}

	fakeBook := testdata.NewBook()
	fakeResponseList := []*response.Book{
		{
			Id:    fakeBook.Id.String(),
			Slug:  fakeBook.Slug,
			Title: fakeBook.Title,
			Subject: response.BookSubject{
				Slug:  fakeBook.Subject.Slug,
				Title: fakeBook.Subject.Title,
			},
			Author:        fakeBook.Author,
			EditionNumber: fakeBook.EditionNumber,
		},
	}

	tests := []struct {
		name string
		args args
		want []*response.Book
	}{
		{
			name: "DomainToRestBookListOK",
			args: args{
				books: []*entity.Book{
					fakeBook,
				},
			},
			want: fakeResponseList,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := response.DomainToRestBookList(tt.args.books); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DomainToRestBookList() = %v, want %v", got, tt.want)
			}
		})
	}
}
