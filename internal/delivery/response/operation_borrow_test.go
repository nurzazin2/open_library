package response_test

import (
	"open_library/domain/entity"
	"open_library/internal/delivery/response"
	"open_library/test/testdata"
	"reflect"
	"testing"
)

func TestDomainToRestOperationBorrow(t *testing.T) {
	type args struct {
		ob *entity.OperationBorrow
	}

	fakeBook := testdata.NewBook()
	fakeOB := testdata.NewOperationBorrow(fakeBook.Title)
	fakeOB.Book = fakeBook
	fakeResponse := &response.OperationBorrow{
		Id: fakeOB.Id.String(),
		Borrower: response.OperationBorrowUser{
			Name: fakeOB.Borrower.Name,
		},
		Book:        response.DomainToRestBook(fakeBook),
		BorrowDate:  fakeOB.BorrowDate,
		SubmittedAt: fakeOB.SubmittedAt,
	}

	tests := []struct {
		name string
		args args
		want *response.OperationBorrow
	}{
		{
			name: "DomainToRestOperationBorrowOK",
			args: args{
				ob: fakeOB,
			},
			want: fakeResponse,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := response.DomainToRestOperationBorrow(tt.args.ob); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DomainToRestOperationBorrow() = %v, want %v", got, tt.want)
			}
		})
	}
}
