package response

import (
	"open_library/domain/entity"
	"time"
)

type OperationBorrowUser struct {
	Name string `json:"name"`
}

type OperationBorrow struct {
	Id          string              `json:"id"`
	Borrower    OperationBorrowUser `json:"borrower"`
	Book        *Book               `json:"book"`
	BorrowDate  time.Time           `json:"borrow_date"`
	SubmittedAt time.Time           `json:"submitted_at"`
}

func DomainToRestOperationBorrow(ob *entity.OperationBorrow) *OperationBorrow {
	return &OperationBorrow{
		Id: ob.Id.String(),
		Borrower: OperationBorrowUser{
			Name: ob.Borrower.Name,
		},
		Book:        DomainToRestBook(ob.Book),
		BorrowDate:  ob.BorrowDate,
		SubmittedAt: ob.SubmittedAt,
	}
}

func DomainToRestOperationBorrowList(borrows []*entity.OperationBorrow) []*OperationBorrow {
	var items []*OperationBorrow

	for _, b := range borrows {
		items = append(items, DomainToRestOperationBorrow(b))
	}

	return items
}
