package response

import "open_library/domain/entity"

type BookSubject struct {
	Slug  string `json:"slug"`
	Title string `json:"title"`
}

type Book struct {
	Id            string      `json:"id"`
	Slug          string      `json:"slug"`
	Title         string      `json:"title"`
	Subject       BookSubject `json:"subject"`
	Author        string      `json:"author"`
	EditionNumber string      `json:"editionNumber"`
}

func DomainToRestBook(book *entity.Book) *Book {
	return &Book{
		Id:    book.Id.String(),
		Slug:  book.Slug,
		Title: book.Title,
		Subject: BookSubject{
			Slug:  book.Subject.Slug,
			Title: book.Subject.Title,
		},
		Author:        book.Author,
		EditionNumber: book.EditionNumber,
	}
}

func DomainToRestBookList(books []*entity.Book) []*Book {
	var items []*Book

	for _, book := range books {
		items = append(items, DomainToRestBook(book))
	}

	return items
}
