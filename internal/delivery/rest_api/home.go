package rest_api

import (
	"net/http"
	"open_library/pkg/utils"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	appInfo := struct {
		Name    string
		Version string
	}{
		Name:    "Open Library",
		Version: "0.1.0.0",
	}
	utils.RespondWithJSON(w, appInfo)
}
