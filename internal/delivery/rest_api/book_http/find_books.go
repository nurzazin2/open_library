package book_http

import (
	"github.com/gorilla/mux"
	"net/http"
	"open_library/internal/delivery/response"
	"open_library/pkg/utils"
)

func (h *bookHandler) FindBooks(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	books, errUC := h.bookUC.FindBookList(vars["subject"])
	if errUC != nil {
		utils.RespondWithError(w, http.StatusBadRequest, []error{errUC})
		return
	}

	utils.RespondWithJSON(w, response.DomainToRestBookList(books))
}
