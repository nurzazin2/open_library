package book_http

import (
	"github.com/gorilla/mux"
	"open_library/domain/usecase"
)

type bookHandler struct {
	bookUC usecase.BookUseCase
}

func BookHandler(r *mux.Router, bookUC usecase.BookUseCase) {
	handler := &bookHandler{
		bookUC: bookUC,
	}

	r.HandleFunc("/books/{subject}", handler.FindBooks).Methods("GET")
}
