package operation_http

import (
	"encoding/json"
	"net/http"
	"open_library/domain/entity"
	"open_library/internal/delivery/request"
	"open_library/internal/delivery/response"
	"open_library/pkg/common"
	"open_library/pkg/utils"
)

func (h *operationHandler) BorrowSubmit(w http.ResponseWriter, r *http.Request) {
	var req request.OperationBorrow
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&req); err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, []error{err})
		return
	}

	operation, errUC := h.operationUC.BorrowBook(entity.OperationBorrowDTO{
		Id:         common.NewID(),
		Borrower:   req.Borrower,
		Book:       req.Book,
		BorrowDate: req.BorrowDate,
	})
	if errUC != nil {
		utils.RespondWithError(w, http.StatusBadRequest, []error{errUC})
		return
	}

	utils.RespondWithJSON(w, response.DomainToRestOperationBorrow(operation))
}
