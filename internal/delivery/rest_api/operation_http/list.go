package operation_http

import (
	"net/http"
	"open_library/internal/delivery/response"
	"open_library/pkg/utils"
)

func (h *operationHandler) BorrowList(w http.ResponseWriter, r *http.Request) {
	borrows, errUC := h.operationUC.BorrowList()
	if errUC != nil {
		utils.RespondWithError(w, http.StatusBadRequest, []error{errUC})
		return
	}

	utils.RespondWithJSON(w, response.DomainToRestOperationBorrowList(borrows))
}
