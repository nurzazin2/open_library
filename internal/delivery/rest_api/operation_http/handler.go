package operation_http

import (
	"github.com/gorilla/mux"
	"open_library/domain/usecase"
)

type operationHandler struct {
	operationUC usecase.OperationUseCase
}

func OperationHandler(r *mux.Router, operationUC usecase.OperationUseCase) {
	handler := &operationHandler{
		operationUC: operationUC,
	}

	r.HandleFunc("/borrow", handler.BorrowList).Methods("GET")
	r.HandleFunc("/borrow", handler.BorrowSubmit).Methods("POST")
}
