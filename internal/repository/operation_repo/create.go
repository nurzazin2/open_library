package operation_repo

import "open_library/domain/entity"

func (o *operationRepository) CreateOperationBorrow(b *entity.OperationBorrow) error {
	o.operations = append(o.operations, b)
	return nil
}
