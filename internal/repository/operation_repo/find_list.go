package operation_repo

import "open_library/domain/entity"

func (o *operationRepository) FindList() ([]*entity.OperationBorrow, error) {
	return o.operations, nil
}
