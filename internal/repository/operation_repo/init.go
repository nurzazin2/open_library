package operation_repo

import (
	"open_library/domain/entity"
	"open_library/domain/repository"
)

type operationRepository struct {
	operations []*entity.OperationBorrow
}

func NewBookRepository() repository.OperationRepo {
	return &operationRepository{}
}
