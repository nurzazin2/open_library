package book_repo

import "open_library/domain/entity"

func (b *bookRepository) FindBook(slug string) (*entity.Book, error) {
	for _, book := range b.books {
		if slug == book.Slug {
			return book, nil
		}
	}
	return nil, nil
}
