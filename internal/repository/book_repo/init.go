package book_repo

import (
	"open_library/domain/entity"
	"open_library/domain/repository"
)

type bookRepository struct {
	books []*entity.Book
}

func NewBookRepository(books []*entity.Book) repository.BookRepo {
	return &bookRepository{books: books}
}
