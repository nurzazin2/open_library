package book_uc

import "open_library/domain/entity"

func (b *bookImplement) FindBookList(subject string) ([]*entity.Book, error) {
	books, _ := b.bookRepo.FindListBook(subject)
	return books, nil
}
