package book_uc

import (
	"open_library/domain/repository"
	"open_library/domain/usecase"
)

type bookImplement struct {
	bookRepo repository.BookRepo
}

func NewBook(bookRepo repository.BookRepo) usecase.BookUseCase {
	return &bookImplement{bookRepo: bookRepo}
}
