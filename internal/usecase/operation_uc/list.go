package operation_uc

import "open_library/domain/entity"

func (o *operationImplement) BorrowList() ([]*entity.OperationBorrow, error) {
	list, _ := o.operationRepo.FindList()
	return list, nil
}
