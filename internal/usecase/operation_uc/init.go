package operation_uc

import (
	"open_library/domain/repository"
	"open_library/domain/usecase"
)

type operationImplement struct {
	operationRepo repository.OperationRepo
	bookRepo      repository.BookRepo
}

func NewOperation(operationRepo repository.OperationRepo, bookRepo repository.BookRepo) usecase.OperationUseCase {
	return &operationImplement{operationRepo: operationRepo, bookRepo: bookRepo}
}
