package operation_uc

import (
	"open_library/domain/entity"
	"open_library/pkg/exceptions"
)

func (o *operationImplement) BorrowBook(dto entity.OperationBorrowDTO) (*entity.OperationBorrow, error) {
	op, err := entity.NewOperationBorrow(dto)
	if err != nil {
		return nil, err
	}

	book, _ := o.bookRepo.FindBook(dto.Book)
	err = op.AddBook(book)
	if err != nil {
		return nil, err
	}

	borrows, _ := o.BorrowList()
	for _, b := range borrows {
		if b.Book.Slug == book.Slug {
			return nil, exceptions.ErrorDataExist("book")
		}
	}

	_ = o.operationRepo.CreateOperationBorrow(op)
	return op, nil
}
