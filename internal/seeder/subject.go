package seeder

import (
	"github.com/gosimple/slug"
	"open_library/domain/entity"
	"open_library/pkg/common"
)

func NewSubject(title string) *entity.Subject {
	return &entity.Subject{
		Id:    common.NewID(),
		Slug:  slug.Make(title),
		Title: title,
	}
}
