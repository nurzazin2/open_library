package seeder

import (
	"github.com/gosimple/slug"
	"open_library/domain/entity"
	"open_library/pkg/common"
)

func GenerateBooks() []*entity.Book {
	const author = "nur zazin"
	const edition = "OL16911237M"
	var books []*entity.Book

	titles := []string{
		"Dracula",
		"Night Shift",
		"Writing About Film",
		"Irish Melodies",
	}

	subjects := []string{
		"Horror",
		"Horror",
		"Film",
		"Music",
	}

	for i, title := range titles {
		books = append(books, &entity.Book{
			Id:            common.NewID(),
			Title:         title,
			Slug:          slug.Make(title),
			Author:        author,
			EditionNumber: edition,
			Subject:       NewSubject(subjects[i]),
		})
	}
	return books
}
