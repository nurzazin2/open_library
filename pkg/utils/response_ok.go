package utils

import (
	"encoding/json"
	"net/http"
)

type jsonResponse struct {
	Data interface{} `json:"data"`
}

func RespondWithJSON(w http.ResponseWriter, payload interface{}) {
	response, _ := json.Marshal(jsonResponse{
		Data: payload,
	})

	w.Header().Set(HEADER_CONTENT_TYPE, HEADER_VALUE_JSON)
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}
