package utils

import (
	"fmt"
	"time"
)

func DateDefault(datetime time.Time) time.Time {
	if datetime.IsZero() {
		return time.Now()
	}

	return datetime
}

func DateToTime(date string) time.Time {
	t, err := time.Parse("2006-01-02 15:04:05", fmt.Sprintf("%s 00:00:00", date))
	if err != nil {
		return time.Time{}
	}
	return t
}

func TimeToDate(t time.Time) string {
	return fmt.Sprintf("%d-%02d-%02d", t.Year(), t.Month(), t.Day())
}
