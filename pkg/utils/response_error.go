package utils

import (
	"encoding/json"
	"net/http"
)

type jsonErrorResponse struct {
	Message string   `json:"message"`
	Errors  []string `json:"errors"`
}

func RespondWithError(w http.ResponseWriter, statusCode int, errors []error) {
	var errStr []string
	for _, err := range errors {
		errStr = append(errStr, err.Error())
	}
	jr := jsonErrorResponse{
		Message: "Error",
		Errors:  errStr,
	}
	response, _ := json.Marshal(jr)

	w.Header().Set(HEADER_CONTENT_TYPE, HEADER_VALUE_JSON)
	w.WriteHeader(statusCode)
	w.Write(response)
}
